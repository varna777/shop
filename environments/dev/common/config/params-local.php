<?php
return [
    'cookieValidationKey' => '',
    'cookieDomain' => '.myshop.com',
    'frontendHost' => 'http://myshop.com',
    'adminHost' => 'http://admin.myshop.com',
    'staticHostInfo' => 'http://static.myshop.com',
];
