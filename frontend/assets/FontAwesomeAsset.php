<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 18.06.2018
 * Time: 9:21
 */
namespace frontend\assets;
use yii\web\AssetBundle;
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/font-awesome';
    public $css = [
        'css/font-awesome.min.css',
    ];
}