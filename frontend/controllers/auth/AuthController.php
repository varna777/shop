<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 15.06.2018
 * Time: 7:26
 */

namespace frontend\controllers\auth;
use core\services\auth\AuthService;
use Yii;
use yii\web\Controller;
use core\forms\auth\LoginForm;


class AuthController extends Controller
{


    private $service;

    public function __construct($id, $module, AuthService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new LoginForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                               $user = $this->service->auth($form);
                               Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
                               return $this->goBack();
            } catch (\DomainException $e) {
                               Yii::$app->errorHandler->logException($e);
                               Yii::$app->session->setFlash('error', $e->getMessage());
                           }
        }

        $this->layout = 'blank';
        return $this->render('login', [
            'model' => $form,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}