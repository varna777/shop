<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 18.06.2018
 * Time: 10:42
 */
namespace frontend\controllers\shop;

use yii\web\Controller;

class CatalogController extends Controller
{
        public $layout = 'catalog';

    public function actionIndex()
    {
         return $this->render('index');
    }
}