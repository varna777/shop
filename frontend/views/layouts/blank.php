<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 18.06.2018
 * Time: 10:01
 */
use common\widgets\Alert;
use yii\widgets\Breadcrumbs;
?>

<?php $this->beginContent('@frontend/views/layouts/main.php') ?>
    <section>
    <div class="container">
    <div class="row">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <div  class="col-sm-12">
                <?= $content ?>
            </div>
    </div>
    </div>
    </section>

    <?php $this->endContent() ?>