<?php

use yii\db\Migration;

/**
 * Class m180616_094403_products_photos
 */
class m180616_094403_products_photos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('{{%product_photos}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('{{%idx-photos-product_id}}', '{{%product_photos}}', 'product_id');
        $this->addForeignKey('{{%fk-shop_photos-product_id}}', '{{%product_photos}}', 'product_id', '{{%products}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_photos}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_094403_products_photos cannot be reverted.\n";

        return false;
    }
    */
}
