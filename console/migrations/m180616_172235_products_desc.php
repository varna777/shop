<?php

use yii\db\Migration;

/**
 * Class m180616_172235_products_desc
 */
class m180616_172235_products_desc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'description', $this->text()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%products}}', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_172235_products_desc cannot be reverted.\n";

        return false;
    }
    */
}
