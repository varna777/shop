<?php

use yii\db\Migration;

/**
 * Class m180616_171414_add_main_photo
 */
class m180616_171414_add_main_photo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'main_photo_id', $this->integer());

                $this->createIndex('{{%idx-products-main_photo_id}}', '{{%products}}', 'main_photo_id');

                $this->addForeignKey('{{%fk-products-main_photo_id}}', '{{%products}}', 'main_photo_id', '{{%product_photos}}', 'id', 'SET NULL', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180616_171414_add_main_photo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_171414_add_main_photo cannot be reverted.\n";

        return false;
    }
    */
}
