<?php

use yii\db\Migration;

/**
 * Class m180616_091559_products_table
 */
class m180616_091559_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'code' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'price_old' => $this->integer(),
            'price_new' => $this->integer(),
            'rating' => $this->decimal(3, 2),
            'meta_json' => $this->text(),
        ], $tableOptions);
        $this->createIndex('{{%idx-shop_products-code}}', '{{%products}}', 'code', true);

        $this->createIndex('{{%idx-shop_products-category_id}}', '{{%products}}', 'category_id');
        $this->addForeignKey('{{%fk-products-category_id}}', '{{%products}}', 'category_id', '{{%product_categories}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_091559_products_table cannot be reverted.\n";

        return false;
    }
    */
}
