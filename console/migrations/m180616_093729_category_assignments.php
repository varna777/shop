<?php

use yii\db\Migration;

/**
 * Class m180616_093729_category_assignments
 */
class m180616_093729_category_assignments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('{{%category_assignments}}', [
            'product_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('{{%pk-category_assignments}}', '{{%category_assignments}}', ['product_id', 'category_id']);

        $this->createIndex('{{%idx-category_assignments-product_id}}', '{{%category_assignments}}', 'product_id');
        $this->createIndex('{{%idx-category_assignments-category_id}}', '{{%category_assignments}}', 'category_id');
        $this->addForeignKey('{{%fk-category_assignments-product_id}}', '{{%category_assignments}}', 'product_id', '{{%products}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-category_assignments-category_id}}', '{{%category_assignments}}', 'category_id', '{{%product_categories}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180616_093729_category_assignments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180616_093729_category_assignments cannot be reverted.\n";

        return false;
    }
    */
}
