<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 15.06.2018
 * Time: 10:13
 */
namespace common\bootstrap;

use yii\base\BootstrapInterface;
use yii\mail\MailerInterface;
use core\services\ContactService;
class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });

        $container->setSingleton(ContactService::class, [], [
                       [$app->params['supportEmail'] => $app->name . ' robot'],
                        $app->params['adminEmail']

                    ]);
    }
}