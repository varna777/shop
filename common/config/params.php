<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'cookieDomain' => '.myshop.com',
    'frontendHost' => 'http://myshop.com',
    'adminHost' => 'http://admin.myshop.com',
    'staticHostInfo' => 'http://static.myshop.com',
    'staticPath' => dirname(__DIR__, 2) . '/static',
];
