<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 13.06.2018
 * Time: 16:09
 */
return [
      'class' => 'yii\web\UrlManager',
      'hostInfo' => $params['adminHost'],
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
          '' => 'site/index',
          '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
          '<_a:login|logout>' => 'auth/<_a>',


          '<_a:[\w\-]+>' => '<_c>/index',
          '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
          '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
          '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
          '<_m:debug>/<_c:\w+>/<_a:\w+>' => '<_m>/<_c>/<_a>'
          ],
   ];