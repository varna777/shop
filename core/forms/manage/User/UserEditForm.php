<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 15.06.2018
 * Time: 23:30
 */
namespace core\forms\manage\User;
use core\entities\User\User;
use yii\base\Model;
class UserEditForm extends Model
{
    public $username;
    public $email;
    public $_user;
    public function __construct(User $user, $config = [])
    {
        $this->username = $user->username;
        $this->email = $user->email;
        $this->_user = $user;
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['username', 'email'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->_user->id]],
        ];
    }
}