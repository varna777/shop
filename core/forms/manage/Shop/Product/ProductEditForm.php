<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 12:02
 */
namespace core\forms\manage\Shop\Product;
use core\entities\Shop\Product;
use core\forms\CompositeForm;
use core\forms\manage\MetaForm;
/**
 * @property MetaForm $meta
 * @property CategoriesForm $categories
 * @property TagsForm $tags
 * @property ValueForm[] $values
 */
class ProductEditForm extends CompositeForm
{
    public $code;
    public $name;
    public $description;
    private $_product;
    public function __construct(Product $product, $config = [])
    {

        $this->code = $product->code;
        $this->name = $product->name;
        $this->description = $product->description;
        $this->meta = new MetaForm($product->meta);
        $this->categories = new CategoriesForm($product);
        $this->_product = $product;
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['code', 'name'], 'required'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetClass' => Product::class, 'filter' => $this->_product ? ['<>', 'id', $this->_product->id] : null],
            ['description', 'string'],
        ];
    }
    protected function internalForms(): array
    {
        return ['meta','categories'];
    }
}