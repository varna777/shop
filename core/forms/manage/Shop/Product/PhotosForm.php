<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 11:56
 */
namespace core\forms\manage\Shop\Product;
use yii\base\Model;
use yii\web\UploadedFile;
class PhotosForm extends Model
{

    public $files;
    public function rules(): array
    {
        return [
            ['files', 'each', 'rule' => ['image']],
        ];
    }
    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->files = UploadedFile::getInstances($this, 'files');
            return true;
        }
        return false;
    }
}