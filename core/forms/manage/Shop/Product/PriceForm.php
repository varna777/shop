<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 11:57
 */

namespace core\forms\manage\Shop\Product;

use core\entities\Shop\Product;
use core\forms\manage\MetaForm;
use yii\base\Model;

class PriceForm extends Model
{
    public $old;
    public $new;
    public function __construct(Product $product = null, $config = [])
    {
        if ($product) {
            $this->new = $product->price_new;
            $this->old = $product->price_old;
        }
        parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['new'], 'required'],
            [['old', 'new'], 'integer', 'min' => 0],
        ];
    }
}
