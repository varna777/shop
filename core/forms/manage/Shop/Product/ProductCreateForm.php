<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 11:58
 */

namespace core\forms\manage\Shop\Product;
use core\entities\Shop\Product;
use core\forms\CompositeForm;
use core\forms\manage\MetaForm;


class ProductCreateForm extends CompositeForm
{

    public $code; // articul
    public $name;
    public $description;
    public function __construct($config = [])
    {
        $this->price = new PriceForm();
        $this->meta = new MetaForm();
        $this->categories = new CategoriesForm();
        $this->photos = new PhotosForm();
         parent::__construct($config);
    }
    public function rules(): array
    {
        return [
            [['code', 'name'], 'required'],
            [['code', 'name'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetClass' => Product::class],
            ['description', 'string'],
        ];
    }
    protected function internalForms(): array
    {
        return ['price', 'meta','photos', 'categories'];
    }
}