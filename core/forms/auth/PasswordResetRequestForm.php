<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 14.06.2018
 * Time: 22:38
 */
namespace core\forms\auth;

use yii\base\Model;
use core\entities\User\User;

class PasswordResetRequestForm extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }
}
