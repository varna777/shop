<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 14.06.2018
 * Time: 22:39
 */
namespace core\forms\auth;

use yii\base\Model;

class ResetPasswordForm extends Model
{
    public $password;

    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }
}