<?php

/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 14.06.2018
 * Time: 15:55
 */
namespace core\services\auth;
use core\entities\User\User;
use core\forms\auth\SignupForm;
class SignupService
{
    public function signup(SignupForm $form): User
    {
        $user = User::signup(
            $form->username,
            $form->email,
            $form->password
        );
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
        return $user;
    }
}
