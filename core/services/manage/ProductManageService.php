<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 12:12
 */
namespace core\services\manage;
use core\entities\Meta;
use core\entities\Shop\Product;
use core\forms\manage\Shop\Product\ProductCreateForm;
use core\forms\manage\Shop\Product\ProductEditForm;
use core\forms\manage\Shop\Product\CategoriesForm;
use core\forms\manage\Shop\Product\PhotosForm;
use core\forms\manage\Shop\Product\PriceForm;
use core\repositories\CategoryRepository;
use core\repositories\ProductRepository;
class ProductManageService
{
    private $products;
    private $categories;
    public function __construct( ProductRepository $products,  CategoryRepository $categories )
    {
        $this->products = $products;
        $this->categories = $categories;
    }
    public function create(ProductCreateForm $form): Product
    {

        $category = $this->categories->get($form->categories->main);
        $product = Product::create(
            $category->id,
            $form->code,
            $form->name,
            $form->description,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        $product->setPrice($form->price->new, $form->price->old);
        foreach ($form->categories->others as $otherId) {
                     $category = $this->categories->get($otherId);
                   $product->assignCategory($category->id);
                    }
        foreach ($form->photos->files as $file) {
                       $product->addPhoto($file);
                    }

        $this->products->save($product);
        return $product;
    }

    public function edit($id, ProductEditForm $form): void
    {
        $product = $this->products->get($id);
        $category = $this->categories->get($form->categories->main);

        $product->edit(
            $form->code,
            $form->name,
            $form->description,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        $product->changeMainCategory($category->id);



            $product->revokeCategories();

            $this->products->save($product);

            foreach ($form->categories->others as $otherId) {
                $category = $this->categories->get($otherId);
                $product->assignCategory($category->id);
            }
            $this->products->save($product);

    }
    public function changePrice($id, PriceForm $form): void
    {
        $product = $this->products->get($id);
        $product->setPrice($form->new, $form->old);
        $this->products->save($product);
    }


    public function addPhotos($id, PhotosForm $form): void
    {
        $product = $this->products->get($id);
        foreach ($form->files as $file) {
            $product->addPhoto($file);
        }
        $this->products->save($product);
    }
    public function movePhotoUp($id, $photoId): void
    {
        $product = $this->products->get($id);
        $product->movePhotoUp($photoId);
        $this->products->save($product);
    }
    public function movePhotoDown($id, $photoId): void
    {
        $product = $this->products->get($id);
        $product->movePhotoDown($photoId);
        $this->products->save($product);
    }
    public function removePhoto($id, $photoId): void
    {
        $product = $this->products->get($id);
        $product->removePhoto($photoId);
        $this->products->save($product);
    }


    public function remove($id): void
    {
        $product = $this->products->get($id);
        $this->products->remove($product);
    }
}