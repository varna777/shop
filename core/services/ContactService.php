<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 15.06.2018
 * Time: 11:06
 */
namespace core\services;
use core\forms\ContactForm;
class ContactService
{
    private $supportEmail;
    private $adminEmail;
    public function __construct($supportEmail, $adminEmail)
    {
        $this->supportEmail = $supportEmail;
        $this->adminEmail = $adminEmail;
    }
    public function send(ContactForm $form): void
    {
        $sent = \Yii::$app->mailer->compose()
            ->setFrom($this->supportEmail)
            ->setTo($this->adminEmail)
            ->setSubject($form->subject)
            ->setTextBody($form->body)
            ->send();
        if (!$sent) {
            throw new \RuntimeException('Sending error.');
        }
    }
}