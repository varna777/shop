<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 15.06.2018
 * Time: 12:20
 */
namespace core\helpers;
use core\entities\User\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
class UserHelper
{
    public static function statusList(): array
    {
        return [
            User::STATUS_DELETED => 'Видалений',
            User::STATUS_ACTIVE => 'Активний',
        ];
    }
    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }
    public static function statusLabel($status): string
    {
        switch ($status) {
            case User::STATUS_DELETED:
                $class = 'label label-danger';
                break;
            case User::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }
        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}