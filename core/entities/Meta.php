<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 0:49
 */
namespace core\entities;

class Meta
{
 public $title;
 public $description;
 public $keywords;

 public function __construct($title, $description, $keywords)
 {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
  }
}