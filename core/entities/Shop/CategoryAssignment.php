<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 12:36
 */
namespace core\entities\Shop;
use yii\db\ActiveRecord;

class CategoryAssignment extends ActiveRecord
{
    public static function create($categoryId): self
    {
        $assignment = new static();
        $assignment->category_id = $categoryId;
        return $assignment;
    }
    public function isForCategory($id): bool
    {
        return $this->category_id == $id;
    }
    public static function tableName(): string
    {
        return '{{%category_assignments}}';
    }
}