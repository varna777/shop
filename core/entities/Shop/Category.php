<?php
/**
 * Created by PhpStorm.
 * User: Varna
 * Date: 16.06.2018
 * Time: 11:01
 */

namespace core\entities\Shop;
use paulzi\nestedsets\NestedSetsBehavior;
use core\entities\behaviors\MetaBehavior;
use core\entities\Meta;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public $meta;
    public static function create($name, $slug, $title, $description, Meta $meta): self
    {
        $category = new static();
        $category->name = $name;
        $category->slug = $slug;
        $category->title = $title;
        $category->description = $description;
        $category->meta = $meta;
        return $category;
    }
    public function edit($name, $slug, $title, $description, Meta $meta): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->title = $title;
        $this->description = $description;
        $this->meta = $meta;
    }
    public static function tableName(): string
    {
        return '{{%product_categories}}';
    }
    public function behaviors(): array
    {
        return [
            MetaBehavior::className(),
            NestedSetsBehavior::className(),
        ];
    }
    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


}